package com.gb.versusapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum MemberRating {
    MEMBER("회원"),
    MANAGEMENT("관리자"),
    STOP_MEMBER("활동중지"),
    OUT_MEMBER("회원탈퇴");

    private final String name;
}
