package com.gb.versusapi.repository;

import com.gb.versusapi.entity.MemberAsk;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberAskRepository extends JpaRepository<MemberAsk, Long> {
}
