package com.gb.versusapi.repository;

import com.gb.versusapi.entity.AskManagement;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AskManagementRepository extends JpaRepository<AskManagement, Long> {
}
