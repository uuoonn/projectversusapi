package com.gb.versusapi.repository;

import com.gb.versusapi.entity.BookmarkTarget;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookmarkTargetRepository extends JpaRepository<BookmarkTarget, Long> {
}
