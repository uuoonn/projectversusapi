package com.gb.versusapi.repository;

import com.gb.versusapi.entity.Member;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
    long countByUsername(String username);
}
