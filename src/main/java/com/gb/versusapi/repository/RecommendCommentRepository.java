package com.gb.versusapi.repository;

import com.gb.versusapi.entity.RecommendComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecommendCommentRepository extends JpaRepository<RecommendComment, Long> {
}
