package com.gb.versusapi.repository;

import com.gb.versusapi.entity.RecommendBoard;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RecommendBoardRepository extends JpaRepository<RecommendBoard, Long> {
}
