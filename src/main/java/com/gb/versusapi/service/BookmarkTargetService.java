package com.gb.versusapi.service;

import com.gb.versusapi.entity.BookmarkTarget;
import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.bookMarkTaget.BookmarkTargetRequest;
import com.gb.versusapi.repository.BookmarkTargetRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class BookmarkTargetService {
    private final BookmarkTargetRepository bookMarkTargetRepository;

    /**
     *
     * 팔로우 추가
     *
     * @param request
     */
    public void setLikeTarget(BookmarkTargetRequest request){
        BookmarkTarget addData = new BookmarkTarget();
        addData.setMemberBookmark(request.getBookmarkTargetId().getMemberBookmark());

    }
}
