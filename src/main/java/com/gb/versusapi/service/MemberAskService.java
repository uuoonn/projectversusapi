package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.MemberAsk;
import com.gb.versusapi.model.memberAsk.MemberAskCreateRequest;
import com.gb.versusapi.model.memberAsk.MemberAskEditRequest;
import com.gb.versusapi.repository.MemberAskRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor

public class MemberAskService {
    private final MemberAskRepository memberAskRepository;


    /**
     *
     * 작성자 아이디 찾아오기
     * @param id 멤버 아이디
     * @return
     */
    public MemberAsk getMemberAskId(long id){
        return memberAskRepository.findById(id).orElseThrow();
    }

    /**
     *
     * 문의글 작성
     *
     *
     * @param member
     * @param request
     */
    public void setMemberAsk (Member member, MemberAskCreateRequest request){
        MemberAsk addData = new MemberAsk();
        addData.setMember(member);
        addData.setTitle(request.getTitle());
        addData.setContent(request.getContent());
        addData.setDateUserAsk(LocalDateTime.now());

        memberAskRepository.save(addData);
    }

    /**
     * 문의글 수정
     *
     * @param id
     * @param request
     */
    public void putAskEdit (long id, MemberAskEditRequest request){
        MemberAsk originData = memberAskRepository.findById(id).orElseThrow();
        originData.setTitle(request.getTitle());
        originData.setContent(request.getContent());
        originData.setDateUserAsk(LocalDateTime.now());

        memberAskRepository.save(originData);

    }

    /**
     * 문의글 삭제
     * @param id
     */
    public void delAsk(long id){
        memberAskRepository.deleteById(id);
    }
}
