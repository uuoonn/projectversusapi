package com.gb.versusapi.service;

import com.gb.versusapi.entity.Board;
import com.gb.versusapi.model.board.OrderLikeItem;
import com.gb.versusapi.model.board.OrderTimeItem;
import com.gb.versusapi.repository.BoardRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import javax.swing.border.Border;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor

public class BoardService {
    private final BoardRepository boardRepository;

    /**
     *
     * 최신 게시글 리스트
     * 정렬 기능 없음
     *
     * @return
     */
    public List<OrderTimeItem> getOrderTimeItems() {
        List<Board> orginList = boardRepository.findAll();

        List<OrderTimeItem> result = new LinkedList<>();
        for (Board board : orginList) {
            OrderTimeItem addData = new OrderTimeItem();
            addData.setId(board.getId());
            addData.setMemberName(board.getMember().getUsername());
            addData.setTopicA(board.getTopicA());
            addData.setTopicB(board.getTopicB());
            addData.setDateBoard(LocalDateTime.now());

            result.add(addData);
        }
        return result;
    }

    /**
     *
     * 추천수 정렬 게시글 리스트
     * 정렬 기능 없음
     *
     * @return
     */
    public List<OrderLikeItem> getOrderLikeItems() {
        List<Board> originList = boardRepository.findAll();
        List<OrderLikeItem> result = new LinkedList<>();
        for(Board board : originList){
            OrderLikeItem addItem = new OrderLikeItem();
            addItem.setMemberId(board.getMember().getUsername());
            addItem.setTopicA(board.getTopicA());
            addItem.setTopicB(board.getTopicB());
            addItem.setCountLike(board.getCountLike());

            result.add(addItem);
        }

        return result;

    }


}
