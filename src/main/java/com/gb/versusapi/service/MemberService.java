package com.gb.versusapi.service;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import com.gb.versusapi.model.member.MemberDupCheckResponse;
import com.gb.versusapi.model.member.MemberJoinRequest;
import com.gb.versusapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

@Service
@RequiredArgsConstructor

public class MemberService {
    private final MemberRepository memberRepository;


    public Member getMemberId(long id){
        return memberRepository.findById(id).orElseThrow();
    }



    /**
     * 회원을 가입시킨다.
     *
     * @throws Exception 아이디 중복일 경우, 비밀번호와 비밀번호 확인이 일치하지 않을 경우
     */

    public void setMemberJoin(MemberJoinRequest request) throws Exception {
        if (!isNewUserName(request.getUsername())) throw new Exception();
        if (!request.getPassword().equals(request.getPasswordRe())) throw new Exception();

        Member member = new Member();

        member.setName(request.getName());
        member.setUsername(request.getUsername());
        member.setPassword(request.getPassword());
        member.setNickName(request.getNickName());
        member.setDateBirth(request.getDateBirth());
        member.setPhoneNumber(request.getPhoneNumber());
        member.setGender(request.getGender());
        member.setEmail(request.getEmail());

        member.setMemberRating(MemberRating.MEMBER);
        member.setGrade(Grade.BRONZE);
        member.setDateUser(LocalDateTime.now());




        memberRepository.save(member);
    }

    /**
     * 아이디 중복값 확인하는 코드 리팩토링
     *
     * @param username 아이디
     * @return 중복 체크한 아이디
     */
    public MemberDupCheckResponse getMemberIdDupCheck(String username) {
        MemberDupCheckResponse response = new MemberDupCheckResponse();
        response.setIsNew(isNewUserName(username));

        return response;
    }

    /**
     * 아이디 중복값을 확인한다.
     *
     * @param username 아이디
     * @return ture 신규 아이디 false 중복 아이디
     */

    private boolean isNewUserName(String username) {
        long dupCount = memberRepository.countByUsername(username);

        return dupCount <= 0;
    }
}
