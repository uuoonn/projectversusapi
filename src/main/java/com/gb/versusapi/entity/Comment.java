package com.gb.versusapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter

public class Comment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "voteBoardId", nullable = false)
    private Member voteBoard;

    @Column(nullable = false, length = 50)
    private String contents;

    @Column(nullable = false)
    private long recommendNum;

    @Column(nullable = false)
    private LocalDateTime dateCommentRegister;


}
