package com.gb.versusapi.entity;

import com.gb.versusapi.enums.Gender;
import com.gb.versusapi.enums.Grade;
import com.gb.versusapi.enums.MemberRating;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private MemberRating memberRating;

    @Column(nullable = false, length = 10)
    @Enumerated(value = EnumType.STRING)
    private Grade grade;

    @Column(nullable = false, length = 15, unique = true)
    private String username;

    @Column(nullable = false)
    private String password;

    @Column(nullable = false, length = 10, unique = true)
    private String nickName;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false)
    private LocalDate dateBirth;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false, length = 5)
    @Enumerated(value = EnumType.STRING)
    private Gender gender;

    @Column(nullable = false, length = 30, unique = true)
    private String email;

    @Column(nullable = false)
    private LocalDateTime dateUser;

}
