package com.gb.versusapi.entity;

import com.gb.versusapi.enums.Category;
import com.gb.versusapi.enums.PostType;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Entity
@Getter
@Setter
public class Board {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "memberId", nullable = false)
    private Member member;

    @Column(nullable = false, length = 15)
    @Enumerated(value = EnumType.STRING)
    private Category category;

//    @Column(nullable = false, length = 10)
//@Enumerated(value = EnumType.STRING)
//    private PostType postType;

    @Column(nullable = false, length = 20)
    private String topicA;

//    @Column(length = 15)
//    private String contentA;

    @Column(nullable = false)
    private Long contentANum;

    @Column(nullable = false, length = 20)
    private String topicB;

//    @Column(length = 15)
//    private String contentB;

    @Column(nullable = false)
    private Long contentBNum;

    @Column(nullable = false)
    private LocalDateTime dateBoard;

    @Column(nullable = false)
    private Long countLike;

    @Column(nullable = false)
    private Long viewCount;

    @Column(nullable = false)
    private Boolean isOverTime;

    @Column(nullable = false)
    private Boolean isSecret;

}
