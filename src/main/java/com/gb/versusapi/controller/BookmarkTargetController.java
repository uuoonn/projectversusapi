package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.model.bookMarkTaget.BookmarkTargetRequest;
import com.gb.versusapi.service.BookmarkTargetService;
import com.gb.versusapi.service.MemberService;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/bookmark-people")
public class BookmarkTargetController {

    private final BookmarkTargetService bookMarkTargetService;
    private final MemberService memberService;

    @PostMapping("/create/member-id-/{memberId}")
    public String setLikeTarget (@PathVariable long memberId, @RequestBody BookmarkTargetRequest request){

        bookMarkTargetService.setLikeTarget(request);

        return "팔로우 완료";
    }
}
