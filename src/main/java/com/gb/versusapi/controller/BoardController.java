package com.gb.versusapi.controller;

import com.gb.versusapi.model.board.OrderLikeItem;
import com.gb.versusapi.model.board.OrderTimeItem;
import com.gb.versusapi.service.BoardService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/board")
public class BoardController {
    private final BoardService boardService;


    @GetMapping("/all")
    public List<OrderTimeItem> getOrderTimeItems(){
        return boardService.getOrderTimeItems();
    }
    @GetMapping("/like-order")
    public List<OrderLikeItem> getOrderLikeItems(){
        return boardService.getOrderLikeItems();
    }

}
