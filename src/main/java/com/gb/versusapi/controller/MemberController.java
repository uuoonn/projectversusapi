package com.gb.versusapi.controller;

import com.gb.versusapi.model.member.MemberDupCheckResponse;
import com.gb.versusapi.model.member.MemberJoinRequest;
import com.gb.versusapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/member")
public class MemberController {

    private final MemberService memberService;

    @PostMapping("/member-new")
    public String setMemberJoin(@RequestBody MemberJoinRequest request) throws Exception {
        memberService.setMemberJoin(request);

        return "회원등록 OK";
    }

    @GetMapping("/id/duplicate/check")
    public MemberDupCheckResponse getMemberIdDupCheck(@RequestParam(name = "username") String username){
        return memberService.getMemberIdDupCheck(username);
    }

}
