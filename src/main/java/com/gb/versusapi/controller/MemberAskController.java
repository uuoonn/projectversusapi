package com.gb.versusapi.controller;

import com.gb.versusapi.entity.Member;
import com.gb.versusapi.entity.MemberAsk;
import com.gb.versusapi.model.memberAsk.MemberAskCreateRequest;
import com.gb.versusapi.model.memberAsk.MemberAskEditRequest;
import com.gb.versusapi.service.MemberAskService;
import com.gb.versusapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/ask-member")



public class MemberAskController {

    private final MemberAskService memberAskService;
    private final MemberService memberService;


    @PostMapping("/ask-new/{memberId}")
    public String setMemberAsk(@PathVariable long memberId, @RequestBody MemberAskCreateRequest request){
        Member member = memberService.getMemberId(memberId);
        memberAskService.setMemberAsk(member,request);


        return "문의글 등록 OK";
    }

    @PutMapping("/ask-edit/memberAsk-id/{memberAskId}")
    public String putAskEdit(@PathVariable long memberAskId, @RequestBody MemberAskEditRequest request){
        memberAskService.putAskEdit(memberAskId,request);


        return "수정 ok";
    }


    @DeleteMapping("/delete/{id}")
    public String delAsk(@PathVariable long id){
        memberAskService.delAsk(id);

        return "문의글 삭제 ok";
    }


}
