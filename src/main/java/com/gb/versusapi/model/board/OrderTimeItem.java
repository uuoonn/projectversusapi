package com.gb.versusapi.model.board;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter

public class OrderTimeItem {
    private Long id;
    private String memberName;
    private String topicA;
    private String topicB;
    private LocalDateTime dateBoard;
}
