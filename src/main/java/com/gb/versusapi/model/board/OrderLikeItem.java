package com.gb.versusapi.model.board;

import com.gb.versusapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class OrderLikeItem {
    private String memberId;
    private String topicA;
    private String topicB;
    private Long countLike;

}
