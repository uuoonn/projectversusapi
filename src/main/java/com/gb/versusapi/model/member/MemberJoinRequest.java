package com.gb.versusapi.model.member;

import com.gb.versusapi.enums.Gender;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter

public class MemberJoinRequest {
    private String username;
    private String password;
    private String passwordRe;
    private String nickName;
    private String name;
    private LocalDate dateBirth;
    private String phoneNumber;
    private Gender gender;
    private String email;

}
