package com.gb.versusapi.model.member;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.repository.cdi.Eager;

@Getter
@Setter

public class MemberDupCheckResponse {
    private Boolean isNew;
}
