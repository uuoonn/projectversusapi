package com.gb.versusapi.model.bookMarkTaget;

import com.gb.versusapi.entity.BookmarkTarget;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BookmarkTargetRequest {
    private BookmarkTarget bookmarkTargetId;
}
