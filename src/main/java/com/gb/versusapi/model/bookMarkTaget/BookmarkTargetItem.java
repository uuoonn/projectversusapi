package com.gb.versusapi.model.bookMarkTaget;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class BookmarkTargetItem {
    private String memberBookmark;
}
