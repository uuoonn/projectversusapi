package com.gb.versusapi.model.memberAsk;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class MemberAskCreateRequest {
    private String title;
    private String content;
}
