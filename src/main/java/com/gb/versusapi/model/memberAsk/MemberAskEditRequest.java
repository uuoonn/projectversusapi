package com.gb.versusapi.model.memberAsk;

import com.gb.versusapi.entity.Member;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Setter
@Getter

public class MemberAskEditRequest {
    private String title;
    private String content;

}
