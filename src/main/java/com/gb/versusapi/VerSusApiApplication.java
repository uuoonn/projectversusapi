package com.gb.versusapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VerSusApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(VerSusApiApplication.class, args);
    }

}
